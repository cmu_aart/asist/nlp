import setuptools
from setup_utils import filter_deps

with open('README.md', 'r') as readme_file:
	long_description = readme_file.read()

extras = {
	'BaseAgent': filter_deps([
		'BaseAgent @ git+https://gitlab.com/cmu_asist/BaseAgent@v0.1.5',
		'MinecraftBridge @ git+https://gitlab.com/cmu_asist/MinecraftBridge@v1.3.7',
		'MinecraftElements @ git+https://gitlab.com/cmu_asist/MinecraftElements@v0.4.6',
	]),
}

setuptools.setup(
	name='asist_nlp',
	version='0.2.2',
	description='Natural language processing for ASIST.',
	long_description=long_description,
	long_description_content_type='text/markdown',
	url='https://gitlab.com/cmu_asist/nlp',
	packages=setuptools.find_packages(),
	classifiers=[
		'Programming Language :: Python :: 3',
		'License :: OSI Approved :: MIT License',
		'operating System :: OS Independent',
	],
	python_requires='>=3.6',
	install_requires=[
		'nltk',
		'pandas',
		'torch>=1.10',
		'transformers',
		'numpy',

		# Non Apple Silicon
		'tensorflow-cpu>=2 ; platform_system != "Darwin" or platform_machine != "arm64"',

		# Apple Silicon
		'tensorflow-macos==2.7.0 ; platform_system=="Darwin" and platform_machine=="arm64"',
		'tensorflow-metal==0.3.0 ; platform_system=="Darwin" and platform_machine=="arm64"',
	],
	extras_require={
        **extras,
        'all': list(set(sum(extras.values(), [])))
    }
)

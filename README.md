
# NLP

## Installation

	git clone https://gitlab.com/cmu_asist/NLP
	cd NLP

To do a basic install:

	pip install --user -e .

To install with all ASIST requirements:

	pip install --user -e .[all]

## ASIST Requirements

This package depends on the following repository:
- [`BaseAgent`](https://gitlab.com/cmu_asist/BaseAgent) (version >= 0.1.5)

## Usage

```
>>> from asist_nlp import NLPManager
>>> manager = NLPManager(agent.context, level_1_path='level_1.params')
```
## Data

Download saved model parameters [from here](https://drive.google.com/drive/folders/1B8Kl3gruat4FZSmIr1cRDRol30movBYS?usp=sharing).

## Messages

Messages are published to the internal Redis bus on the `nlp` channel by the NLPManager whenever a communication is observed. Every message contains level 1 (type) and level 2 (content) codes predicted from NLP models.

The message data is a dictionary that contains four fields:
- `utterance_id`: a unique identifier for the transcribed utterance
- `participant_id`: the ID of the participant who gave the utterence
- `time`: the corresponding mission time, in seconds
- `text`: the text of the utterance
- `codes`: a dictionary of codes outputted from the NLP classifiers, currently with the following keys:
	- `level_1`: one of `{'Intent', 'Question', 'Agreement/Closed-loop', 'Command', 'Inform', 'Other'}`, representing the utterance type
	- `level_2`: a list of codes from `{'tools', 'role', 'rubble', 'markers', 'victim'}`, representing the utterance content

An example message is shown below:

```
>>> msg.data
{
	'utterance_id': 'f2182076-379d-43b8-9ce2-cca958f60e8d',
	'participant_id': 'P000101',
	'time': 420.0, 
	'text': "There's a critical in room 101", 
	'codes': {
		'level_1': 'Inform', 'level_2': ['victim'],
	}
}
```

import torch
import numpy as np

import sys
#sys.path.append("/home/aishwarya/")
from .DialogTag.dialog_tag_new import DialogTag



class BertModel(torch.nn.Module):
    def __init__(self, diag, input_size=768, target_size=10):
        super(BertModel, self).__init__()
        bert = diag.return_pymodel()
        self.tokenizer=diag.return_tokenizer()
        size1 = input_size
        size2 = target_size
        size3=128
        self.x1 = bert
        self.dropout = torch.nn.Dropout(0.5)
        self.x2 = torch.nn.Linear(size1, size3, bias=True)
        self.relu = torch.nn.ReLU()
        #self.x3 = torch.nn.Linear(size3, size2, bias=True)
        self.x3 = torch.nn.Linear(size1, size2, bias=True)
        
       

    def forward(self, input_ids, attention_mask):
        #with torch.no_grad():
        outputs=self.x1(input_ids, attention_mask=attention_mask)
        last_hidden_state_cls = outputs[0][:, 0, :]
        #print("last_hidden_state_cls",last_hidden_state_cls.size())
        x = last_hidden_state_cls
        #x = self.dropout(x)
        #x = self.x2(x)
#         x = self.relu(x)
#         x = self.dropout(x)
        output = self.x3(x)
        return output

class Level1Model:
    def __init__(self,param_file, device=None):
        diag=DialogTag('bert-base-uncased')
        self.tokenizer = diag.return_tokenizer()
        self.code_to_label={0: 'Intent', 1: 'Question', 2: 'Agreement/Closed-loop', 3: 'Command', 4: 'Inform', 5: 'Other'}
        #self.code_to_label = code_to_label
        self.param_file = param_file
        self.bert_model = BertModel(diag,target_size=len(self.code_to_label))
        if not device:
            device = torch.device('cuda:1') if torch.cuda.is_available() else torch.device('cpu')
        checkpoint = torch.load(self.param_file, map_location=device)
        self.bert_model.load_state_dict(checkpoint['state_dict'])
        self.device = device
        self.bert_model.to(self.device)
        
        
        
    def predict(self,sentence):
        self.bert_model.eval() 
        with torch.no_grad():
            train_encodings = self.tokenizer(sentence, truncation=True, padding=True)
            input_ids = torch.tensor(train_encodings['input_ids']).to(self.device)
            attention_mask = torch.tensor(train_encodings['attention_mask']).to(self.device)
            output = self.bert_model(input_ids, attention_mask=attention_mask)
            y_pred_softmax = torch.log_softmax(output, dim = 1)
            _, pred_y = torch.max(y_pred_softmax, dim = 1) 

            prediction=[self.code_to_label[c] for c in pred_y.cpu().detach().numpy()]
        return prediction
    
    
    
if __name__=='__main__':
    param_file = '/data/aishwarya/final_models/endtoend_v1.params'
    model = Level1Model(param_file)
    sentence=["I will switch to the searcher instead of heavy"]
    print(model.predict(sentence))

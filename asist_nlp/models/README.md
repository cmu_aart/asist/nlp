# To predict level 1 codes such as Question, Command, Agreement, Inform, Intent, and Other

1. Install Transformers
2. Run following commands to load the model:

    ```
    import level1_code

    param_file = '/data/aishwarya/final_models/endtoend_v1.params'

    model = level1_code.Level1Model(param_file)

    ```
    The model file is present at the location `/data/aishwarya/final_models/endtoend_v1.params` on Ripley

3. Then run following commands to generate the output:

    Example 1
    ```
    sentence=[" I switched to the searcher instead of heavy"]
    print(model.predict(sentence))

    ```
    output : ['Inform']

    Example 2
    ```
    sentence=[" I will switch to the searcher instead of heavy"]
    print(model.predict(sentence))

    ```
    output : ['Intent']

    Example 3
    ```
    sentence=[" Did you switch to the searcher instead of heavy"]
    print(model.predict(sentence))

    ```
    output : ['Question']

    Example 4
    ```
    sentence=["Yes, I did"]
    print(model.predict(sentence))

    ```
    output : ['Agreement/Closed-loop']


The part of this code is taken from `https://github.com/bhavitvyamalik/DialogTag`

# To predict level 2 codes such as tools,  victim,	rubble,	markers, role and no_label
Method 1:

1. Install NLTK
2. Run following commands 

    Example 1

    ```
    import level2_code

    sentence=["I will switch to the searcher instead of heavy."]

    print(level2_code.predict(sentence))

    ```

    output: [['role']]

    Example 2

    ```
    import level2_code

    sentence=["To find victims, I'm labeling as many doors as I can"]

    level2_code.predict(sentence)

    ```

    output: [['markers', 'victim']]

Method 2: Improved Marker predicion

```
    import level2_code

    sentence=["I will switch to the searcher instead of heavy."]

    print(level2_code.predict_improved_markers(sentence))

    ```
    
# Predict changed/novel marker semantics

Method 1: Based completely on keyword matching

```
import marker_semantics
label =  marker_semantics.predict_1(['input utterance'])

```

Method 2: Based on  keyword matching and BERT model to predict markers

```
import marker_semantics
label =  marker_semantics.predict_2(['input utterance'])

```
Method 3: Based on BERT model that can directly predict if marker semantics are changed

```
    import marker_semantics

    param_file = '/data/aishwarya/final_models/semendtoend_best_1.params'

    model = marker_semantics.MarkerSemModel(param_file)

    output = model.predict(['input utterance'])
```


# Predict if the team member is asking for help to understand team coordination


```
    import ishelp

    param_file = '/data/aishwarya/final_models/ishelpendtoend_best_1.params'

    model = ishelp.ishelpModel(param_file)

    output = model.predict(['input utterance'])
```


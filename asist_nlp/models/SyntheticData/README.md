# Synthetic Language Data

This folder contains codes and data for synthetic language data of transcripts. Basically, we want to generate more transcipts by BERT-based model to improve the performance of down-stream NLP tasks.

## Installation Instruction

The package we use is the Huggingface library, which you can install by
```
pip install transformers
```

You can also clone its repo, by
```
git clone https://github.com/huggingface/transformers.git
```

The original fine-tune and generation code can be found in `transformers/examples/pytorch`, but I have already copied and modified them to fit our task, called `run_language_modeling.py` and `run_generation.py`.

## Folder Structure
```
transcripts_train.txt           9000 training transcripts
transcripts_test.txt            1000 test transcripts
transcripts_test_gen.txt        generated transcripts based on test transcripts' head
GeneratedTranscripts5000.txt    5000 Generated transcripts

run_language_modeling.py        fine-tune on GPT-2 model
run_generation.py               generate transcripts
preprocess.py                   pre-process on existing transcripts
evaluation.py                   evaluate on generated dataset
```

## How to run
running run_language_model.py can fine-tune a pre-trained GPT-2 model with your own dataset, following the ways in run.sh.

With your fine-tuned model, running run_generation.py can generate transcripts, following the ways in generate.sh.


## Result

GPT-2 pretrained model fine-tuned on 9000 training transcripts:

    Perplexity on test transcripts: 14.870 

    BLEU score: 0.202 (for test transcirpts, generate based on the first three words)
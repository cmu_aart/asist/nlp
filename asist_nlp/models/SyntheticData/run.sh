N=7
OUTPUT_DIR=~/output
TRAIN_FILE=~/transcripts.txt
CUDA_VISIBLE_DEVICES=$N python3 run_language_modeling.py \
--output_dir=$OUTPUT_DIR \
--model_type=gpt2 \
--model_name_or_path=gpt2 \
--do_train \
--train_data_file=$TRAIN_FILE \
--per_device_train_batch_size=64 \
--line_by_line \
--learning_rate 5e-5 \
--num_train_epochs=5
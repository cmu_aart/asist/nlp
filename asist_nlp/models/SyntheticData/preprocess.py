# -*- coding: utf-8 -*-
"""
Created on Sun Oct 24 23:31:57 2021

@author: han1h
"""


import os
import pandas

folder = r'~/HSR Zoom Transcripts'
transcript_files = []
transcripts = ''
bos_token = '<BOS>'
eos_token = '<EOS>'

for root, dirs, files in os.walk(folder):
    for name in files:
        transcript_files.append(os.path.join(folder, name))
        
for file in transcript_files:
    cvs_file = pandas.read_csv(file)
    cvs_file_corrections = list(cvs_file.iloc[:,1])
    
    for line in cvs_file_corrections:
        if type(line) == str and ord(line[0]) > 57:
            if ":" in line:
                transcript_line = line.split(":")[1][1:]
            else:
                transcript_line = line
            transcripts += bos_token + ' ' + transcript_line + ' ' + eos_token + '\n'

f = open(r"~/transcripts_train.txt", "w")
f.write(transcripts)

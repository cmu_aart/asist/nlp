TEST_FILE=~/transcripts_test.txt
CUDA_VISIBLE_DEVICES=7 python3 run_language_modeling.py \
--output_dir=/home/haochenh/output \
--model_type=gpt2 \
--model_name_or_path=/home/haochenh/output \
--do_eval \
--eval_data_file=$TEST_FILE \
--per_device_eval_batch_size=2 \
--line_by_line
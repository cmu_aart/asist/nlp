# Train BERT model to predict high level and low level codes

```
Input variables:
train_x : List of utterances for training 
train_y : List of lables for training 
dev_x: List of utterances for validation 
dev_y:  List of lables for validation 

```

# Train model for high level codes

```
python org_hsr_level_all_out.py --codes high

```



# Train model for topic level codes

```
python org_hsr_level_all_out.py --codes topic

```
